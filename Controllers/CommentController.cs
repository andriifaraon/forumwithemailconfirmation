﻿using GeeksForLess_Forum.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;

namespace GeeksForLess_Forum.Controllers
{
    public class CommentController : Controller
    {
        private readonly ForumContext _context;
        public CommentController(ForumContext context) => _context = context;
        public IActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public IActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> Create(string text)
        {
            Comment comment = new Comment() { Text = text };
            if (ModelState.IsValid)
            {
                
                User owner = await _context.Users.FirstOrDefaultAsync(u => u.Email == TempData["UserEmail"].ToString());
                TempData.Keep();

                if (owner != null)
                    comment.User = owner;

                int selectedTopic = (int)TempData["SelectedTopic"];
                TempData.Keep();

                Topic topic = await _context.Topics.FirstOrDefaultAsync(t => t.Id == selectedTopic);
                if (topic != null)
                    comment.Topic = topic;

                _context.Comments.Add(comment);
                _context.SaveChanges();

                return RedirectToAction("Details", "Topic", new { Id = selectedTopic });
            }
            return View(comment);
        }

        public async Task<IActionResult> Remove(int? Id)
        {
            if (Id == null)
            {
                return new StatusCodeResult(404);
            }
            Comment comment = await _context.Comments.FindAsync(Id);
            if (comment == null)
            {
                return NotFound();
            }

            _context.Comments.Remove(comment);
            _context.SaveChanges();

            int selectedTopic = (int)TempData["SelectedTopic"];
            TempData.Keep();

            return RedirectToAction("Details", "Topic", new { Id = selectedTopic });
        }


        public async Task<IActionResult> Edit(int? Id)
        {
            if (Id == null)
            {
                return NotFound();
            }

            var comment = await _context.Comments.FindAsync(Id);
            if (comment == null)
            {
                return NotFound();
            }
            return View(comment);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int Id, [Bind("Id, User, UserEmail, Topic, TopicId, Text")] Comment comment)
        {
            if (Id != comment.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(comment);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!_context.Comments.Contains(comment))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                int selectedTopic = (int)TempData["SelectedTopic"];
                TempData.Keep();

                return RedirectToAction("Details", "Topic", new { Id = selectedTopic });
            }
            return View(comment);
        }
    }
}
