﻿using GeeksForLess_Forum.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;

namespace GeeksForLess_Forum.Controllers
{
    public class TopicController : Controller
    {
        private readonly ForumContext _context;
        public TopicController(ForumContext context) => _context = context;
        public IActionResult Index()
        {
            var topics = _context.Topics.Select(t => t);
            return View(topics.ToList());
        }

        [HttpGet]
        public IActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> Create(Topic topic)
        {
            if (ModelState.IsValid)
            {
                User owner = await _context.Users.FirstOrDefaultAsync(u => u.Email == TempData["UserEmail"].ToString());
                TempData.Keep();

                if (owner != null)
                    topic.User = owner;

                _context.Topics.Add(topic);
                _context.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(topic);
        }

        public async Task<IActionResult> Details(int? Id)
        {
            if (Id == null)
            {
                return NotFound();
            }

            var topic = await _context.Topics.FirstOrDefaultAsync(m => m.Id == Id);
            if (topic == null)
            {
                return NotFound();
            }

            TempData["SelectedTopic"] = topic.Id;

            return View((topic, new Comment()));
        }

        public async Task<IActionResult> Edit(int? Id)
        {
            if (Id == null)
            {
                return NotFound();
            }

            var topic = await _context.Topics.FindAsync(Id);
            if (topic == null)
            {
                return NotFound();
            }
            return View(topic);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int Id, [Bind("Id, User, UserEmail, Name, Text")] Topic topic)
        {
            if (Id != topic.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(topic);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!_context.Topics.Contains(topic))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(topic);
        }

        public async Task<IActionResult> Remove(int? Id)
        {
            if (Id == null)
            {
                return new StatusCodeResult(404);
            }
            Topic topic = await _context.Topics.FindAsync(Id);
            if (topic == null)
            {
                return NotFound();
            }

            _context.Topics.Remove(topic);
            _context.SaveChanges();

            return RedirectToAction("Index", "Topic");
        }
    }
}
