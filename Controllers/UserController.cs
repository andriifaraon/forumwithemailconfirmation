﻿using GeeksForLess_Forum.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GeeksForLess_Forum.Configuration;
using GeeksForLess_Forum.EmailInteractor;
using Microsoft.AspNetCore.Authorization;

namespace GeeksForLess_Forum.Controllers
{
    public class UserController : Controller
    {
        private readonly ForumContext _context;
        public UserController(ForumContext context) => _context = context;

        [HttpGet]
        public IActionResult Register()
        {
            return View();
        }
        public IActionResult Index()
        {
            return View();
        }

        public IActionResult SuccessfulRegister()
        {
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> Register(User user)
        {
            if (ModelState.IsValid)
            {
                if (_context.Users.Contains(user))
                {
                    if (_context.Users.First(u => u.Email == user.Email).Password == user.Password)
                    {
                        TempData["UserEmail"] = user.Email;
                        return RedirectToAction("Index", "Topic");
                    }
                    else
                        return RedirectToAction("Register", "User");
                }
                    

                StringBuilder link = new StringBuilder(URI.LocalLink);
                string emailForLink = user.Email.Replace("@", "%40");
                link.Append("/User/ConfirmEmail?Email=" + emailForLink + "&Password=" + user.Password);

                EmailService emailService = new EmailService();
                await emailService.SendEmailAsync("smtptest2021pz@gmail.com", "2021smpT", user.Email, "Confirm your account", 
                        $"Please confirm your registartion by following such link: <a href='{link}'>link</a>",
                        "Email Confirmation", "smtp.gmail.com", 465);

                return RedirectToAction("SuccessfulRegister", "User");
            }
            return View(user);
        }

        [HttpGet]
        [AllowAnonymous]
        public async Task<IActionResult> ConfirmEmail(string Email, string Password)
        {
            if (Email == null || Password == null)
            {
                return View("Error");
            }
            await _context.AddAsync(new User() { Email = Email, Password = Password });
            _context.SaveChanges();

            TempData["UserEmail"] = Email;

            return RedirectToAction("Index", "Topic");
        }
    }
}
