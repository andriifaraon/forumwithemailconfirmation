﻿using MimeKit;
using System;
using System.Collections.Generic;
using System.Linq;
using MailKit.Net.Smtp;
using System.Threading.Tasks;

namespace GeeksForLess_Forum.EmailInteractor
{
    public class EmailService
    {
        //senderEmail = smtptest2021pz@gmail.com
        //senderPassword = 2021smpT
        //host = smtp.gmail.com
        //port = 465
        //topic = Email Confirmation
        public async Task SendEmailAsync(string senderEmail, string senderPassword, 
            string receiverEmail, string subject, string message, string topic, string host, int port)
        {
            var emailMessage = new MimeMessage();

            emailMessage.From.Add(new MailboxAddress(topic, senderEmail));
            emailMessage.To.Add(new MailboxAddress("", receiverEmail));
            emailMessage.Subject = subject;
            emailMessage.Body = new TextPart(MimeKit.Text.TextFormat.Html) { Text = message };

            using var client = new SmtpClient();

            await client.ConnectAsync(host, port, true);
            await client.AuthenticateAsync(senderEmail, senderPassword);
            await client.SendAsync(emailMessage);

            await client.DisconnectAsync(true);
        }
    }
}
