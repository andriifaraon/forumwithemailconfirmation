#pragma checksum "D:\Metanit\GeeksForLess_Forum\Views\Topic\Index.cshtml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "5c8b1f38bcdbb45a388d3e5146599d40845b07aa"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(AspNetCore.Views_Topic_Index), @"mvc.1.0.view", @"/Views/Topic/Index.cshtml")]
namespace AspNetCore
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
#nullable restore
#line 1 "D:\Metanit\GeeksForLess_Forum\Views\_ViewImports.cshtml"
using GeeksForLess_Forum;

#line default
#line hidden
#nullable disable
#nullable restore
#line 2 "D:\Metanit\GeeksForLess_Forum\Views\_ViewImports.cshtml"
using GeeksForLess_Forum.Models;

#line default
#line hidden
#nullable disable
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"5c8b1f38bcdbb45a388d3e5146599d40845b07aa", @"/Views/Topic/Index.cshtml")]
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"d80dfd291c4d090779ff77700e6403955c68cb63", @"/Views/_ViewImports.cshtml")]
    public class Views_Topic_Index : global::Microsoft.AspNetCore.Mvc.Razor.RazorPage<IEnumerable<GeeksForLess_Forum.Models.Topic>>
    {
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
            WriteLiteral("\r\n");
#nullable restore
#line 3 "D:\Metanit\GeeksForLess_Forum\Views\Topic\Index.cshtml"
  
    ViewBag.Title = "Articles";

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n<h2>All Our Articles</h2>\r\n\r\n<p>\r\n    <table class=\"table\">\r\n        <tr>\r\n            <th>\r\n                ");
#nullable restore
#line 13 "D:\Metanit\GeeksForLess_Forum\Views\Topic\Index.cshtml"
           Write(Html.DisplayNameFor(model => model.Name));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n            </th>\r\n            <th>\r\n                ");
#nullable restore
#line 16 "D:\Metanit\GeeksForLess_Forum\Views\Topic\Index.cshtml"
           Write(Html.DisplayNameFor(model => model.UserEmail));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n            </th>\r\n            <th></th>\r\n        </tr>\r\n\r\n");
#nullable restore
#line 21 "D:\Metanit\GeeksForLess_Forum\Views\Topic\Index.cshtml"
         foreach (var item in Model)
        {

#line default
#line hidden
#nullable disable
            WriteLiteral("            <tr>\r\n                <td>\r\n                    ");
#nullable restore
#line 25 "D:\Metanit\GeeksForLess_Forum\Views\Topic\Index.cshtml"
               Write(Html.DisplayFor(modelItem => item.Name));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n                </td>\r\n                <td>\r\n                    ");
#nullable restore
#line 28 "D:\Metanit\GeeksForLess_Forum\Views\Topic\Index.cshtml"
               Write(Html.DisplayFor(modelItem => item.UserEmail));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n                </td>\r\n\r\n                <td>\r\n                    ");
#nullable restore
#line 32 "D:\Metanit\GeeksForLess_Forum\Views\Topic\Index.cshtml"
               Write(Html.ActionLink("Read", "Details", new { id = item.Id }));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n");
#nullable restore
#line 33 "D:\Metanit\GeeksForLess_Forum\Views\Topic\Index.cshtml"
                     if (item.UserEmail == TempData["UserEmail"].ToString())
                    {
                        TempData.Keep();

#line default
#line hidden
#nullable disable
            WriteLiteral("                        <span> - </span>\r\n");
#nullable restore
#line 37 "D:\Metanit\GeeksForLess_Forum\Views\Topic\Index.cshtml"
                   Write(Html.ActionLink("Edit ", "Edit", new { id = item.Id }));

#line default
#line hidden
#nullable disable
            WriteLiteral("                        <span> - </span>\r\n");
#nullable restore
#line 39 "D:\Metanit\GeeksForLess_Forum\Views\Topic\Index.cshtml"
                   Write(Html.ActionLink("Delete", "Remove", new { id = item.Id }));

#line default
#line hidden
#nullable disable
#nullable restore
#line 39 "D:\Metanit\GeeksForLess_Forum\Views\Topic\Index.cshtml"
                                                                                  
                    }
                    else
                        TempData.Keep();

#line default
#line hidden
#nullable disable
            WriteLiteral("                </td>\r\n            </tr>\r\n");
#nullable restore
#line 45 "D:\Metanit\GeeksForLess_Forum\Views\Topic\Index.cshtml"
        }

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n    </table>\r\n    <hr />\r\n    <div class=\"container\">\r\n        <div class=\"row\">\r\n            <div class=\"col text-center\">\r\n                <button");
            BeginWriteAttribute("onclick", " onclick=\"", 1484, "\"", 1554, 4);
            WriteAttributeValue("", 1494, "location.href=\'", 1494, 15, true);
#nullable restore
#line 52 "D:\Metanit\GeeksForLess_Forum\Views\Topic\Index.cshtml"
WriteAttributeValue("", 1509, Url.Action("Create", "Topic"), 1509, 30, false);

#line default
#line hidden
#nullable disable
            WriteAttributeValue("", 1539, "\';return", 1539, 8, true);
            WriteAttributeValue(" ", 1547, "false;", 1548, 7, true);
            EndWriteAttribute();
            WriteLiteral(" class=\"btn btn-primary\">\r\n                    Create New Topic\r\n                </button>\r\n            </div>\r\n        </div>\r\n    </div>\r\n    \r\n\r\n");
        }
        #pragma warning restore 1998
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<IEnumerable<GeeksForLess_Forum.Models.Topic>> Html { get; private set; }
    }
}
#pragma warning restore 1591
