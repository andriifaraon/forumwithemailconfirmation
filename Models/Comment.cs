﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace GeeksForLess_Forum.Models
{
    public class Comment
    {
        [Key]
        public int Id { get; set; }

        [Display(Name = "Comment")]
        public string Text { get; set; }
        public string UserEmail { get; set; }
        public virtual User User { get; set; }
        public int TopicId { get; set; }
        public virtual Topic Topic { get; set; }
    }
}
