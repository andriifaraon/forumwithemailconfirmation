﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GeeksForLess_Forum.Models
{
    public class ForumContext : DbContext
    {
        public DbSet<User> Users { get; set; }
        public DbSet<Topic> Topics { get; set; }
        public DbSet<Comment> Comments { get; set; }
        public ForumContext(DbContextOptions options) : base(options)
        {
            
            //Database.EnsureDeleted();
            //Database.EnsureCreated();
        }
    }
}
