﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace GeeksForLess_Forum.Models
{
    public class Topic
    {
        [Key]
        public int Id { get; set; }
        [Required]
        [Display(Name = "Name of Article")]
        public string Name { get; set; }
        public string Text { get; set; }
        [Display(Name = "Author")]
        public string UserEmail { get; set; }
        public virtual User User { get; set; }
        public virtual List<Comment> Comments { get; set; } = new List<Comment>();
    }
}
